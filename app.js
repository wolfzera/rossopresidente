'use strict';

const express     = require('express');
const app         = express();
const mongoose    = require('mongoose');
const bodyParser  = require('body-parser');
const jwt         = require('jsonwebtoken');
const cors        = require('cors');
const routes      = require('./routes/index');
const config      = require('./configs/index');

//mongoose connection
mongoose.connect(config.db);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('secret', config.secret);
//routes
app.use('/api/v1/', cors(), routes.Deputy);
app.use('/api/v1/', cors(), routes.User);
app.use('/api/v1/', cors(), routes.Login);

app.listen(3000, function() {
  console.log('server running in port: 3000');
});
