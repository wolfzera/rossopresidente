'use strict';

exports.Deputy = require('./deputy.router');
exports.User = require('./user.router');
exports.Login = require('./login.router');
