'use strict';

const router  =  require('express').Router();
const ctrl    =  require('../controllers/index');
const auth    = require('../middlewares/index').auth;

router.post('/user', ctrl.User.create);

module.exports = router;
