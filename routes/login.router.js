'use strict';

const router  =  require('express').Router();
const ctrl    =  require('../controllers/index');

router.post('/login', ctrl.Login.auth);

module.exports = router;
