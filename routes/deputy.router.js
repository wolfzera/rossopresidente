'use strict';

const router  =  require('express').Router();
const ctrl    =  require('../controllers/index');
const auth    = require('../middlewares/index').auth;

router.get('/deputy', auth, ctrl.Deputy.findAll);
router.get('/deputy/:id', auth, ctrl.Deputy.findOne);
router.post('/deputy', auth, ctrl.Deputy.create);
router.delete('/deputy', auth, ctrl.Deputy.delete);

module.exports = router;
