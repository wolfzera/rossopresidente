'use strict';

const mongoose  = require('mongoose');
const Schema    = mongoose.Schema;

const userSchema =  new Schema({
  name: { type: String, riquired: true },
  email: { type: String, riquired: true },
  username: { type: String, riquired: true },
  password: { type: String, riquired: true },
  role: { type: String, riquired: true },
  created_at: { type: Date, default : Date.now },
});

module.exports = mongoose.model('User', userSchema);
