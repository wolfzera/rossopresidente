'use strict';

const mongoose  = require('mongoose');
const Schema    = mongoose.Schema;

const deputySchema =  new Schema({
  name: { type: String, riquired: true },
  photo: { type: String },
  broken: { type: String, riquired: true },
  phone: { type: String, riquired: true },
  state: { type: String, riquired: true },
  profession: { type: String, riquired: true },
  votes: { type: Number, riquired: true },
  number_of_mandates : { type: Number, riquired: true },
  main_flags : { type: String },
  biography : { type: String },
  created_at: { type: Date, default : Date.now },
});

module.exports = mongoose.model('Deputy', deputySchema);
