'use strict';

const Users = require('../../models/index').User;
const bcrypt  = require('bcryptjs');
const secret = require('../../configs/index').secret;
const cript = bcrypt.genSaltSync(10);

function createUser(req, res) {
  Users.findOne({username: req.body.username})
  .then(user => {
    if(!user) {

      if(req.body.key === secret) {
        let password = bcrypt.hashSync(req.body.password, cript);

        let user = {
          name: req.body.name,
          email: req.body.email,
          username: req.body.username,
          password: password,
          role: req.body.email,
        };

        Users.create(user)
        .then(response => {
          res.sendStatus(201);
        })
        .catch(error => {
          res.sendStatus(400);
        });

      } else {
        res.sendStatus(401)
      }


    } else {
      res.json({message: "usuario já existe"})
    }
  })
  .catch(err => {
    res.sendStatus(400)
  })
};

module.exports = createUser;
