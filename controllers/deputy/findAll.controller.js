'use strict';

const Deputy = require('../../models/index').Deputy;

function findAllDeputy (req, res) {
  Deputy.find({})
  .then(response => {
    res.status(200).json(response);
  })
  .catch(err => {
    res.status(404).json({err : err});
  });
};

module.exports = findAllDeputy;
