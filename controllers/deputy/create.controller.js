'use strict';

const Deputy = require('../../models/index').Deputy;

function createDeputy (req, res) {
  // res.json(req.body);
  console.log(req.body);
  Deputy.create(req.body)
    .then(response => {
      res.status(201).json(response);
    })
    .catch(err => {
      res.status(404).json({err : err});
    });
};

module.exports = createDeputy;
