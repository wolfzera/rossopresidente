'use strict';

exports.create = require('./create.controller');
exports.findAll = require('./findAll.controller');
exports.findOne = require('./findOne.controller');
exports.delete = require('./delete.controller');
