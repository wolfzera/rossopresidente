'use strict';

const Deputy = require('../../models/index').Deputy;

function findOneDeputy (req, res) {
  Deputy.findOne({_id : req.params.id})
  .then(response => {
    console.log(response);
    res.status(200).json(response);
  })
  .catch(err => {
    res.status(404).json({err : err});
  });
};

module.exports = findOneDeputy;
