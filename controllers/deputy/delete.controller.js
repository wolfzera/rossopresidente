'use strict';

const Deputy = require('../../models/index').Deputy;

function deleteDeputy (req, res) {
  Deputy.remove({_id : req.params.id})
  .then(response => {
    res.status(200).json(response);
  })
  .catch(err => {
    res.status(404).json({err : err});
  });
};

module.exports = deleteDeputy;
