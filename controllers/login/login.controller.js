'use strict';

const jwt = require('jsonwebtoken');
const User = require('../../models/index').User;
const bcrypt  = require('bcryptjs');
const secret = require('../../configs/index').secret;
const cript = bcrypt.genSaltSync(10);
const hash = bcrypt.hashSync(secret, cript);

function login(req, res, next) {
  console.log(req.body);
  User.findOne({email : req.body.email})
  .then(response => {
    // res.json(response)
    function comparePassword() {
      return bcrypt.compareSync(req.body.password, response.password)
    }

    if(comparePassword()) {
      var token = jwt.sign({
        data: response
      }, secret, { expiresIn: 60 * 60 });
      res.json({
        token: token,
        user: {
          nome: response.nome,
          username: response.username,
          email: response.email,
          role: response.role
        }
      })
    } else {
      res.status(401).json({"Mensage": "Dados incorretos"})
    }


  })
  .catch(err => {
    res.status(401).json({"Message" : "Usuário não existe!"});
  });


}

module.exports = login;
