'use strict';

const jwt = require('jsonwebtoken');
const config = require('../configs/index');

function auth(req, res, next) {
  jwt.verify(req.headers.authorization, config.secret, function(err, decoded) {
    if(err) {
      return res.sendStatus(401);
    } else {
      return next();
    }
  });
}

module.exports = auth;
